package ru.t1.gorodtsova.tm.api.repository;

import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;

public interface IUserOwnedRepository<M extends AbstractUserOwnedModel> extends IRepository<M> {

    void clear(String userId);

    boolean existsById(String userId, String id);

    List<M> findAll(String userId);

    List<M> findAll(String userId, Comparator<M> comparator);

    M findOneById(String userId, String id);

    M findOneByIndex(String userId, Integer index);

    int getSize(String userId);

    M removeById(String userId, String id);

    M removeByIndex(String userId, Integer index);

    M add(String userId, M model);

    M remove(String userId, M model);

}
