package ru.t1.gorodtsova.tm.api.service;

import ru.t1.gorodtsova.tm.api.repository.IUserOwnedRepository;
import ru.t1.gorodtsova.tm.enumerated.Sort;
import ru.t1.gorodtsova.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}
