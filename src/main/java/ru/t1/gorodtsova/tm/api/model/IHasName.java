package ru.t1.gorodtsova.tm.api.model;

public interface IHasName {

    String getName();

    void setName(String name);

}
