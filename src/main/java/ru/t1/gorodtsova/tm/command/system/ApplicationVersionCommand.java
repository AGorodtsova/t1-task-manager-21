package ru.t1.gorodtsova.tm.command.system;

public final class ApplicationVersionCommand extends AbstractSystemCommand {

    private final String ARGUMENT = "-v";

    private final String DESCRIPTION = "Show application version";

    private final String NAME = "version";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[VERSION]");
        System.out.println("1.21.0");
    }

}
