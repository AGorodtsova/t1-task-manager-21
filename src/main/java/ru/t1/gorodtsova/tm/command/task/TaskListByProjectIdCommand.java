package ru.t1.gorodtsova.tm.command.task;

import ru.t1.gorodtsova.tm.model.Task;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

import java.util.List;

public final class TaskListByProjectIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Show task list by project id";

    private final String NAME = "task-show-by-project-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[TASK LIST BY PROJECT ID]");
        System.out.println("ENTER PROJECT ID:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = getTaskService().findAllByProjectId(getUserId(), projectId);
        renderTasks(tasks);
    }

}
