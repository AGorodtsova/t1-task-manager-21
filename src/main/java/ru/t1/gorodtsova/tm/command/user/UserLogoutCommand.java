package ru.t1.gorodtsova.tm.command.user;

import ru.t1.gorodtsova.tm.enumerated.Role;

public final class UserLogoutCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Logout current user";

    private final String NAME = "logout";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER LOGOUT]");
        serviceLocator.getAuthService().logout();
    }

    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
