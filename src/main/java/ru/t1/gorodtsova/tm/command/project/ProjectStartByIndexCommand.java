package ru.t1.gorodtsova.tm.command.project;

import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class ProjectStartByIndexCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "Start project by index";

    private final String NAME = "project-start-by-index";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[START PROJECT BY INDEX]");
        System.out.println("ENTER INDEX:");
        final Integer index = TerminalUtil.nextNumber() - 1;
        getProjectService().changeProjectStatusByIndex(getUserId(), index, Status.IN_PROGRESS);
    }

}
