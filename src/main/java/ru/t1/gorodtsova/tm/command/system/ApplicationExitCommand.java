package ru.t1.gorodtsova.tm.command.system;

public final class ApplicationExitCommand extends AbstractSystemCommand {

    private final String DESCRIPTION = "Close application";

    private final String NAME = "exit";

    @Override
    public String getArgument() {
        return null;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
