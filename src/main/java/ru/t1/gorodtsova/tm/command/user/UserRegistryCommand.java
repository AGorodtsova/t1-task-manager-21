package ru.t1.gorodtsova.tm.command.user;

import ru.t1.gorodtsova.tm.api.service.IAuthService;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "Registry user";

    private final String NAME = "user-registry";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        authService.registry(login, password, email);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
