package ru.t1.gorodtsova.tm.command.task;

import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "Remove task by id";

    private final String NAME = "task-remove-by-id";

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE TASK BY ID]");
        System.out.println("ENTER ID");
        final String id = TerminalUtil.nextLine();
        getTaskService().removeById(getUserId(), id);
    }

}
