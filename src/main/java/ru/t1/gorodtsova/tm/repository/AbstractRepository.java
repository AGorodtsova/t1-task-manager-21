package ru.t1.gorodtsova.tm.repository;

import ru.t1.gorodtsova.tm.api.repository.IRepository;
import ru.t1.gorodtsova.tm.exception.entity.ModelNotFoundException;
import ru.t1.gorodtsova.tm.model.AbstractModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    protected final List<M> records = new ArrayList<>();

    @Override
    public M add(final M model) {
        records.add(model);
        return model;
    }

    @Override
    public List<M> findAll() {
        return records;
    }

    @Override
    public List<M> findAll(final Comparator<M> comparator) {
        return records
                .stream()
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    public void clear() {
        records.clear();
    }

    @Override
    public M findOneById(final String id) {
        return records
                .stream()
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public M findOneByIndex(final Integer index) {
        return records.get(index);
    }

    @Override
    public M remove(final M model) {
        records.remove(model);
        return model;
    }

    @Override
    public M removeById(final String id) {
        final M model = findOneById(id);
        if (model == null) throw new ModelNotFoundException();
        remove(model);
        return model;
    }

    @Override
    public M removeByIndex(final Integer index) {
        final M model = findOneByIndex(index);
        if (model == null) throw new ModelNotFoundException();
        remove(model);
        return model;
    }

    @Override
    public void removeAll(Collection<M> collection) {
        records.removeAll(collection);
    }

    @Override
    public boolean existsById(final String id) {
        return findOneById(id) != null;
    }

    @Override
    public int getSize() {
        return records.size();
    }

}
