package ru.t1.gorodtsova.tm.component;

import ru.t1.gorodtsova.tm.api.repository.ICommandRepository;
import ru.t1.gorodtsova.tm.api.repository.IProjectRepository;
import ru.t1.gorodtsova.tm.api.repository.ITaskRepository;
import ru.t1.gorodtsova.tm.api.repository.IUserRepository;
import ru.t1.gorodtsova.tm.api.service.*;
import ru.t1.gorodtsova.tm.command.AbstractCommand;
import ru.t1.gorodtsova.tm.command.project.*;
import ru.t1.gorodtsova.tm.command.system.*;
import ru.t1.gorodtsova.tm.command.task.*;
import ru.t1.gorodtsova.tm.command.user.*;
import ru.t1.gorodtsova.tm.enumerated.Role;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.gorodtsova.tm.exception.system.CommandNotSupportedException;
import ru.t1.gorodtsova.tm.model.Project;
import ru.t1.gorodtsova.tm.model.User;
import ru.t1.gorodtsova.tm.repository.CommandRepository;
import ru.t1.gorodtsova.tm.repository.ProjectRepository;
import ru.t1.gorodtsova.tm.repository.TaskRepository;
import ru.t1.gorodtsova.tm.repository.UserRepository;
import ru.t1.gorodtsova.tm.service.*;
import ru.t1.gorodtsova.tm.util.TerminalUtil;

public final class Bootstrap implements IServiceLocator {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IUserRepository userRepository = new UserRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ILoggerService loggerService = new LoggerService();

    private final IUserService userService = new UserService(userRepository);

    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskListByProjectIdCommand());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserRegistryCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserViewProfileCommand());
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

    private void initDemoData() {
        final User test = userService.create("test", "test", "test@test.ru");
        final User user = userService.create("user", "user", "user@user.ru");
        final User admin = userService.create("admin", "admin", Role.ADMIN);

        projectService.add(test.getId(), new Project("TEST PROJECT", Status.IN_PROGRESS));
        projectService.add(test.getId(), new Project("DEMO PROJECT", Status.NOT_STARTED));

        taskService.create(test.getId(), "Mega task");
        taskService.create(test.getId(), "Beta task");
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    public void start(String[] args) {
        processArguments(args);
        initDemoData();
        initLogger();
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("ENTER COMMAND:");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final RuntimeException e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void processArguments(final String[] args) {
        try {
            if (args == null || args.length == 0) return;
            processArgument(args[0]);
            System.exit(0);
        } catch (final RuntimeException e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
            System.exit(1);
        }
    }

    public void processArgument(final String argument) {
        final AbstractCommand abstractCommand = commandService.getCommandByArgument(argument);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(argument);
        abstractCommand.execute();
    }

    public void processCommand(final String command) {
        final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    private void registry(final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

}
